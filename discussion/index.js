//Javascript Synchronous and Asynchronous

//Single thread
console.log('Hello world!');
console.log('Hello from the other side!');
console.log('Ciao!');

//This will take some time to process, this will result in code "blocking"

// for(let i = 0; i <= 1500; i++) {
// 	console.log(i)
// };

console.log('Loop done!');

//Asynchronous - we can proceed to execute other statements, while time consuming code is running in the backgorund.

//Getting all posts

console.log(fetch('http://jsonplaceholder.typicode.com/posts'))

// Checking the status of the request
fetch('http://jsonplaceholder.typicode.com/posts').then(response => console.log(response.status));

//Async and Await

async function fetchData(){
	let result = await fetch('http://jsonplaceholder.typicode.com/posts')
	console.log(result);

	console.log(typeof result);

	console.log(result.body);

	let json = await result.json();
	console.log(json);
}

fetchData();

//Get a specific post

fetch('http://jsonplaceholder.typicode.com/posts/23').then(response => response.json()).then((json) => console.log(json))

//Create a post

fetch('http://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'New Post',
		body: 'I am a new post',
		userId: 1
	})

})
.then((response) => response.json());
.then((json) => console.log(json));

//Updating a post
fetch('http://jsonplaceholder.typicode.com/posts', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated Post',
		body: "This is an updated post",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//Updating post using PATCH method

fetch('http://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'Correct post'
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//Delete a post
fetch('http://jsonplaceholder.typicode.com/posts/12', {
	method: 'DELETE'
});

//Filtering post

fetch('http://jsonplaceholder.typicode.com/posts?userId=1&userId=10')
.then((response) => response.json())
.then((json) => console.log(json));

//Retrieving specific comments
fetch('http://jsonplaceholder.typicode.com/posts/12/comments')
.then((response) => response.json())
.then((json) => console.log(json))