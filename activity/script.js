fetch('http://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => {
	let titles = json.map(x => (x.title))
	console.log(titles)
});


fetch('http://jsonplaceholder.typicode.com/todos/54')
.then(response => response.json())
.then((json) => console.log(json))

fetch('http://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => {
	let completedandTitles = json.map(x => {
		let completedandTitle = {
			completed: x.completed,
			title: x.title
		} 
		return completedandTitle;
	})
	console.log(completedandTitles)
});

fetch('http://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'New To-Do 1/26/22',
		body: 'Finish the activity',
		userId: 1
	})

})
.then((response) => response.json());
.then((json) => console.log(json));

fetch('http://jsonplaceholder.typicode.com/todos', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updating my to-do list',
		description: "this is for updating my todo list",
		status: "in progress",
		dateCompleted: "to be completed",
		body: "I have updated my to-do list",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('http://jsonplaceholder.typicode.com/todos/25', {
	method: 'PATCH',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'Final update for to-do list',
		completed: "completed",
		statusChanged: "Jan 26 2022"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('http://jsonplaceholder.typicode.com/todos/16', {
	method: 'DELETE'
});


